package com.karolpiotrowicz.supertemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupertemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupertemplateApplication.class, args);
	}
}
