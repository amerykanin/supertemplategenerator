package com.karolpiotrowicz.supertemplate.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class GenerationController {

    @GetMapping("/hello")
    public String hello() {
        return "Karolz";
    }

}
